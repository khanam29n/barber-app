import React, { Component } from "react";
import { BrowserRouter as Router, Routes, Route, Navigate} from "react-router-dom";
import "./../src/Style/Elegant_font_icons.css";
import "./../src/Style/Elegant_line_icons.css";
import "./../src/Style/themify-icons.css";
import "./../src/Style/Barber_icon.css";
import "./../src/Style/Bootstrap_min.css";
import "./../src/Style/Animate_min.css";
import "./../src/Style/Nice_select.css";
import "./../src/Style/owl.carousel.css";
import "./../src/Style/slicknav.min.css";
import "./../src/main.css";
import "./../src/Style/Responsive.css";
import Home from "./Pages/Home";
import About from "./Pages/About";
import Services from "./Pages/Services";
import Blog from "./Pages/Blog";
import Contact from "./Pages/Contact";
import Gallery from "./Pages/Gallery";
import Pricing from "./Pages/Pricing";
import OurTeam from "./Pages/OurTeam";
import Login from "./Pages/Login";
import BlogDetails from "./Pages/BlogDetails";
import NotFoundPage from "./Pages/NotFoundPage";
import Recentpost from "./Components/Recentpost";
import MyProfile from "./Pages/MyProfile";


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: false, 
    };
  }

  
  componentDidMount() {
    const authToken = localStorage.getItem('token');
    if (authToken) {
      this.setState({ isAuthenticated: true });
    }
  }

  authResolver = (element) => {
    console.log("this.state.isAuthenticated",this.state.isAuthenticated);
    console.log("element",element);
    return this.state.isAuthenticated ? element : <Navigate to="/login" />;
  };


  render() {
    const { isAuthenticated } = this.state;
    console.log("isAuthenticated",isAuthenticated)
    return (
      <Router>
        
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/about" element={<About />} />
            <Route exact path="/services" element={<Services />} />
            {/* <Route exact path="/pages" element={<Pages />}></Route>*/}
            <Route exact path="/blog" element={<Blog />} />
            <Route exact path="/contact" element={<Contact />} />
            <Route exact path="/gallery" element={<Gallery />} />
            <Route exact path="/pricing" element={<Pricing />} />
            <Route exact path="/ourteam" element={<OurTeam />} />
            <Route exact path="/*" element={<NotFoundPage />} />
            
            <Route exact path="/recentpost" element={<Recentpost />} />
            <Route exact path="/blogdetails" element={<BlogDetails />} />
            <Route
              path="/myprofile"
              element={isAuthenticated ? <MyProfile /> : <Navigate to="/login" />}
            />
            <Route
              path="/login"
              element={!isAuthenticated ? <Login /> : <Navigate to="/myprofile" />}
            />
            <Route
              path="/logout"
              element={isAuthenticated ? <MyProfile /> : <Navigate to="/login" />}
            />
         
          </Routes>
      </Router>



      
    );
  }
}

export default App;
