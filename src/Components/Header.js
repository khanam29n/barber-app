import React from "react";
import { Link } from "react-router-dom";

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoggedIn: false
    };
  }

  componentDidMount(){
    const authToken = localStorage.getItem('token');
    if (authToken) {
      this.setState({ isLoggedIn: true });
    }
  }

  handleLogout = () => {
    // Implement your logout logic here
    // For this example, we will simply remove the login status from localStorage.
   // 
    localStorage.removeItem("token");
    this.setState({ isLoggedIn: false });
    window.location.assign("http://localhost:3000");
  };

  render() {
    const { isLoggedIn } = this.state;

    return (
      <header id="header" className="header-section">
        <div className="container">
          <nav className="navbar">
            <Link to="/">
              <img src={require("./../img/logo.png")} alt="Barbershop" />
            </Link>
            <div className="d-flex menu-wrap align-items-center">
              <div id="mainmenu" className="mainmenu">
                <ul className="nav">
                  <li>
                    <Link to="/">Home</Link>
                  </li>
                  
                  <li>
                    <Link to="/about">About</Link>
                  </li>
                  <li>
                    <Link to="/services">Services</Link>
                  </li>
                  <li>
                    <Link to="/blog">Blog</Link>
                  </li>
                  <li>
                    <Link to="/contact">Contact</Link>
                  </li>
                  <li>
                    <Link to="/gallery">Gallery</Link>
                  </li>
                  <li>
                    <Link to="/pricing">Pricing</Link>
                  </li>
                  
                  <li>
                    <Link to="/ourteam">OurTeam</Link>
                  </li>
                  {isLoggedIn ? (
                    <>
                      <li>
                        <Link to="/myprofile">MyProfile</Link>
                      </li>
                      <li>
                        <Link to="/logout" onClick={this.handleLogout}>
                          Logout
                        </Link>
                      </li>
                    </>
                  ) : (
                    <li>
                      <Link to="/login">Login</Link>
                    </li>
                  )}
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </header>
    );
  }
}

export default Header;
