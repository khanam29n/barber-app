import React, { Component } from 'react';

class PageHeader extends Component {
  render() {
    return (
      <section className="page_header d-flex align-items-center">
        <div className="container">
          <div
            className="section_heading text-center mb-40 wow fadeInUp"
            data-wow-delay="300ms"
          >
            <h3>Trendy Salon &amp; Spa</h3>
            <h2>Barbershop News</h2>
            <div className="heading-line"></div>
          </div>
        </div>
      </section>
    );
  }
}

export default PageHeader;
