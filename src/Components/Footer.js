import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <>
        <section className="widget_section padding">
          <div className="container">
            <div className="row">
              <div className="col-lg-3 col-md-6 sm-padding">
                <div className="footer_widget">
                  <img
                    className="mb-15"
                    src={require("./../img/logo.png")}
                    alt="Brand"
                  />
                  <p>
                    Our barbershop is the created for men who appreciate premium
                    quality, time and flawless look.
                  </p>
                  <ul className="widget_social">
                    <li>
                      <a href="#">
                        <i className="social_facebook"></i>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="social_twitter"></i>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="social_googleplus"></i>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="social_instagram"></i>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="social_linkedin"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 sm-padding">
                <div className="footer_widget">
                  <h3>Headquaters</h3>
                  <p>962 Fifth Avenue, 3rd Floor New York, NY10022</p>
                  <p>
                    Hello@dynamiclayers.net <br />
                    (+123) 456 789 101
                  </p>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 sm-padding">
                <div className="footer_widget">
                  <h3>Opening Hours</h3>
                  <ul className="opening_time">
                    <li>Monday - Friday 11:30am - 2:008pm</li>
                    <li>Saturday – Monday: 9am – 8pm</li>
                    <li>Monday - Friday 5:30am - 11:008pm</li>
                    <li>Saturday - Sunday 4:30am - 1:00pm</li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-3 col-md-12 sm-padding">
                <div className="footer_widget">
                  <h3>Subscribe to our contents</h3>
                  <div className="subscribe_form">
                    <form action="#" className="subscribe_form">
                      <input
                        type="email"
                        name="email"
                        id="subs-email"
                        className="form_input"
                        placeholder="Email Address..."
                      />
                      <button type="submit" className="submit">
                        SUBSCRIBE
                      </button>
                      <div className="clearfix"></div>
                      <div id="subscribe-result">
                        <p className="subscription-success"></p>
                        <p className="subscription-error"></p>
                      </div>
                    </form>
                  </div>
                  {/* <!-- Subscribe Form --> */}
                </div>
              </div>
            </div>
          </div>
        </section>
        <footer className="footer_section">
          <div className="container">
            <div className="row">
              <div className="col-md-6 xs-padding">
                <div className="copyright">
                  &copy;{" "}
                  <script type="text/javascript">
                    {" "}
                    document.write(new Date().getFullYear())
                  </script>{" "}
                  Barber Shop Powered by DynamicLayers
                </div>
              </div>
              <div className="col-md-6 xs-padding">
                <ul className="footer_social">
                  <li>
                    <a href="#">Orders</a>
                  </li>
                  <li>
                    <a href="#">Terms</a>
                  </li>
                  <li>
                    <a href="#">Report Problem</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </>
    );
  }
}

export default Footer;
