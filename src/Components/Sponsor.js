import React, { Component } from 'react';

class Sponsor extends Component {
  render() {
    return (
      <div className="sponsor_section bg-grey padding">
        <div className="container">
          <ul id="sponsor_carousel" className="sponsor_items owl-carousel">
            <li className="sponsor_item">
              <img src={require("./../img/sponsor-1.png")} alt="sponsor-image" />
            </li>
            <li className="sponsor_item">
              <img src={require("./../img/sponsor-2.png")} alt="sponsor-image" />
            </li>
            <li className="sponsor_item">
              <img src={require("./../img/sponsor-3.png")} alt="sponsor-image" />
            </li>
            <li className="sponsor_item">
              <img src={require("./../img/sponsor-4.png")} alt="sponsor-image" />
            </li>
            <li className="sponsor_item">
              <img src={require("./../img/sponsor-5.png")} alt="sponsor-image" />
            </li>
            <li className="sponsor_item">
              <img src={require("./../img/sponsor-1.png")} alt="sponsor-image" />
            </li>
            <li className="sponsor_item">
              <img src={require("./../img/sponsor-2.png")} alt="sponsor-image" />
            </li>
            <li className="sponsor_item">
              <img src={require("./../img/sponsor-3.png")} alt="sponsor-image" />
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default Sponsor;
