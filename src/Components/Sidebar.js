import React, { Component } from 'react';

class Sidebar extends Component {
  render() {
    return (
      <div className="sidebar-wrap">
        <div className="widget-content">
          <form action="" className="search-form">
            <input type="text" className="form-control" placeholder="Type here" />
            <button className="search-btn" type="button">
              <i className="fa fa-search"></i>
            </button>
          </form>
        </div>
        <div className="widget-content">
          <h4>Categories</h4>
          <ul className="widget-links">
            <li>
              <a href="#">Architecture</a>
            </li>
            <li>
              <a href="#">Interior Design</a>
            </li>
            <li>
              <a href="#">Designing</a>
            </li>
            <li>
              <a href="#">Construction</a>
            </li>
            <li>
              <a href="#">Buildings</a>
            </li>
          </ul>
        </div>
        {/* <!--categories--> */}
        <div className="widget-content">
          <h4>Recent Posts</h4>
          <ul className="thumb-post">
            <li>
              <img src={require("./../img/post-1.jpg")} alt="post" />
              <a href="#">Minimalist trending in modern architecture 2019</a>
            </li>
            <li>
              <img src={require("./../img/post-2.jpg")} alt="post" />
              <a href="#">Terrace in the town kentaro design workshop.</a>
            </li>
            <li>
              <img src={require("./../img/post-3.jpg")} alt="post" />
              <a href="#">W270 house são arquitetos fabio architeqture.</a>
            </li>
          </ul>
        </div>
        {/* <!--tag clouds--> */}
        <div className="widget-content">
          <h4>Tag Clouds</h4>
          <ul className="tags">
            <li>
              <a href="#">Architecture</a>
            </li>
            <li>
              <a href="#">Interior Design</a>
            </li>
            <li>
              <a href="#">Designing</a>
            </li>
            <li>
              <a href="#">Construction</a>
            </li>
            <li>
              <a href="#">Buildings</a>
            </li>
            <li>
              <a href="#">Industrial</a>
            </li>
            <li>
              <a href="#">Factory</a>
            </li>
            <li>
              <a href="#">Material</a>
            </li>
          </ul>
        </div>
        {/* <!--tag clouds--> */}
      </div>
    );
  }
}

export default Sidebar;
