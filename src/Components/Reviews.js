import React, { Component } from 'react';

class Reviews extends Component {
  render() {
    return (
      <section id="reviews" className="testimonial_section padding">
        <div className="container">
          <ul id="testimonial_carousel" className="testimonial_items owl-carousel">
            <li className="testimonial_item">
              <p>
                "There are design companies, and then there are user experience
                design interface design
                <br /> professional. By far one of the worlds best known brands."
              </p>
              <h4 className="text-white">Anita Tran, IT Solutions.</h4>
            </li>
            <li className="testimonial_item">
              <p>
                "There are design companies, and then there are user experience
                design interface design
                <br /> professional. By far one of the worlds best known brands."
              </p>
              <h4 className="text-white">Leslie Williamson, Developer.</h4>
            </li>
            <li className="testimonial_item">
              <p>
                "There are design companies, and then there are user experience
                design interface design
                <br /> professional. By far one of the worlds best known brands."
              </p>
              <h4 className="text-white">Fred Moody, Network Software.</h4>
            </li>
          </ul>
        </div>
      </section>
    );
  }
}

export default Reviews;
