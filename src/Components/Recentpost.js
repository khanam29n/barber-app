import React, { Component } from 'react';

class Recentpost extends Component {
  Posts = [
    {
      image: require("./../img/post-1.jpg"),
      title: "Minimalist trending in modern architecture 2019",
    },
    {
      image: require("./../img/post-2.jpg"),
      title: "Terrace in the town kentaro design workshop.",
    },
    {
      image: require("./../img/post-3.jpg"),
      title: "W270 house são arquitetos fabio architeqture.",
    },
  ];

  tagClouds = [
    "Architecture",
    "Interior Design",
    "Designing",
    "Construction",
    "Buildings",
    "Industrial",
    "Factory",
    "Material",
  ];

  render() {
    return (
      <div className="sidebar-wrap">
        <div className="widget-content">
          <h4>Recent Posts</h4>
          <ul className="thumb-post">
            {this.Posts.map((post, index) => (
              <li key={index}>
                <img src={post.image} alt="post" />
                <a href="#">{post.title}</a>
              </li>
            ))}
          </ul>
        </div>
        <div className="widget-content">
          <h4>Tag Clouds</h4>
          <ul className="tags">
            {this.tagClouds.map((tag, index) => (
              <li key={index}>
                <a href="#">{tag}</a>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default Recentpost;
