import React, { Component } from 'react';

class BookSection extends Component {
  render() {
    return (
      <section className="book_section padding">
        <div className="book_bg"></div>
        <div className="map_pattern"></div>
        <div className="container">
          <div className="row">
            <div className="col-md-6 offset-md-6">
              <form
                action="appointment.php"
                method="post"
                id="appointment_form"
                className="form-horizontal appointment_form"
              >
                <div className="book_content">
                  <h2>Make an appointment</h2>
                  <p>
                    Barber is a person whose occupation is mainly to cut dress groom <br />
                    style and shave men's and boys hair.
                  </p>
                </div>
                <div className="form-group row">
                  <div className="col-md-6 padding-10">
                    <input
                      type="text"
                      id="app_name"
                      name="app_name"
                      className="form-control"
                      placeholder="Name"
                      required
                    />
                  </div>
                  <div className="col-md-6 padding-10">
                    <input
                      type="email"
                      id="app_email"
                      name="app_email"
                      className="form-control"
                      placeholder="Your Email"
                      required
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col-md-6 padding-10">
                    <input
                      type="text"
                      id="app_phone"
                      name="app_phone"
                      className="form-control"
                      placeholder="Your Phone No"
                      required
                    />
                  </div>
                  <div className="col-md-6 padding-10">
                    <input
                      type="text"
                      id="app_free_time"
                      name="app_free_time"
                      className="form-control"
                      placeholder="Your Free Time"
                      required
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col-md-6 padding-10">
                    <select className="form-control" id="app_services" name="app_services">
                      <option>Services</option>
                      <option>Hair Styling</option>
                      <option>Shaving</option>
                      <option>Face Mask</option>
                      <option>Hair Wash</option>
                      <option>Beard Triming</option>
                    </select>
                  </div>
                  <div className="col-md-6 padding-10">
                    <select className="form-control" id="app_barbers" name="app_barbers">
                      <option>Choose Barbers</option>
                      <option>Michel Brown</option>
                      <option>Jonathan Smith</option>
                      <option>Jack Tosan</option>
                      <option>Martin Lane</option>
                    </select>
                  </div>
                </div>
                <button id="app_submit" className="default_btn" type="submit">
                  Make Appointment
                </button>
                <div id="msg-status" className="alert" role="alert"></div>
              </form>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default BookSection;
