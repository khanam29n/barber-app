import React from "react";
import axios from "axios";

import Header from "./../Components/Header";
import Footer from "./../Components/Footer";

const BASE_URL = "http://localhost:8081/api/login"; 
class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      loginError: "",
      loggedIn: false,
    };
  }

  handleInputChange = (event) => {
    const { name, value } = event.target;
    if (name === "username") {
      this.setState({ username: value });
    } else if (name === "password") {
      this.setState({ password: value });
    }
  };

  handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await axios.post(BASE_URL, {
        username: this.state.username,
        password: this.state.password,
      });
      const token = response.data.token;
      // Assuming the API returns an access token upon successful login.
      // You can store the access token in the browser's local storage or a state management library like Redux.
      console.log("Login Successful! Access Token:", token);

      // Set the login status to true in localStorage
      localStorage.setItem("token", token);

      // Redirect to Home page after successful login
      window.location.assign("http://localhost:3000");
    } catch (error) {
      // Handle login error
      this.setState({
        loginError: "Invalid username or password. Please try again.",
      });
    }
  };

  handleLogout = () => {
    // Implement your logout logic here
    // For this example, we will simply set the login status to false.
    this.setState({ loggedIn: false });
  };

  render() {
    const {
      username,
      password,
      loginError,
      loggedIn,
    } = this.state;

    const authFormStyles = {
      width: "300px",
      margin: "0 auto",
      padding: "20px",
      border: "1px solid #ccc",
      borderRadius: "5px",
    };

    const containerStyles = {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      textAlign: "center",
      marginTop: "80px",
    };

    const labelStyles = {
      display: "block",
      marginBottom: "5px",
    };

    const inputStyles = {
      width: "100%",
      padding: "5px",
      marginBottom: "5px",
      border: "1px solid #ccc",
      borderRadius: "3px",
    };

    const buttonStyles = {
      display: "block",
      width: "100%",
      padding: "10px",
      backgroundColor: "#9e8a78",
      color: "white",
      border: "none",
      borderRadius: "3px",
      cursor: "pointer",
      marginBottom: "15px",
    };

    return (
      <div>
        <Header />
        <div style={authFormStyles}>
          <h2 style={containerStyles}>Login</h2>
          {loginError && <p style={{ color: "red" }}>{loginError}</p>}
          {loggedIn ? (
            // If logged in, show the logout button
            <button
              type="button"
              style={buttonStyles}
              onClick={this.handleLogout}
            >
              Logout
            </button>
          ) : (
            // If not logged in, show the login form
            <form onSubmit={this.handleSubmit}>
              <label style={labelStyles}>
                Username:
                <input
                  type="text"
                  name="username"
                  style={inputStyles}
                  value={username}
                  onChange={this.handleInputChange}
                />
              </label>
              <br />
              <label style={labelStyles}>
                Password:
                <input
                  type="password"
                  name="password"
                  value={password}
                  style={inputStyles}
                  onChange={this.handleInputChange}
                />
              </label>
              <br />
              <button type="submit" style={buttonStyles}>
                Submit
              </button>
            </form>
          )}
        </div>
        <Footer />
      </div>
    );
  }
}

export default Login;