import React from "react";
import Header from "./../Components/Header";
import AboutUs from "./../Components/AboutUs";
import Service from "../Components/Service";
import TeamSection from "../Components/TeamSection";
import CtaSection from "../Components/CtaSection";
// import Sponsor from "../Components/Sponsor";
import Footer from "./../Components/Footer";

class About extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <section className="page_header d-flex align-items-center">
          <div className="container">
            <div
              className="section_heading text-center mb-40 wow fadeInUp"
              data-wow-delay="300ms"
            >
              <h3>Trendy Salon &amp; Spa</h3>
              <h2>Our Barbershop</h2>
              <div className="heading-line"></div>
            </div>
          </div>
        </section>
        <AboutUs />
        <Service />
        <TeamSection />
        <CtaSection />
        {/* <Sponsor /> */}
        <Footer />
      </div>
    );
  }
}

export default About;
