import React, { Component } from 'react';
import Header from './../Components/Header';
import PageHeader from './../Components/PageHeader';
import CtaSection from './../Components/CtaSection';
import Footer from './../Components/Footer';
import './../main.css';
import './../Style/Animate_min.css';
import './../Style/Barber_icon.css';
import './../Style/Bootstrap_min.css';
import './../Style/Button.css';
import './../Style/Elegant_font_icons.css';
import './../Style/Elegant_line_icons.css';
import './../Style/Nice_select.css';
import './../Style/owl.carousel.css';
import './../Style/Responsive.css';
import './../Style/slicknav.min.css';
import './../Style/themify-icons.css';

class Gallery extends Component {
  render() {
    return (
      <div>
        <Header />
        <PageHeader />
        <section id="gallery" className="gallery_section bg-grey bd-bottom padding">
          <div className="container">
            <ul className="gallery_filter mb-30">
              <li className="active" data-filter="*">
                All
              </li>
              <li data-filter=".branding">Haircut</li>
              <li data-filter=".website">Face Masking</li>
              <li data-filter=".print">Shaving</li>
              <li data-filter=".photo">Hair Color</li>
            </ul>
            <ul className="portfolio_items row">
              <li className="col-lg-6 col-md-6 padding-15 single_item branding">
                <figure className="portfolio_item">
                  <img src={require('./../img/hero-1.jpg')} alt="Portfolio Item" />
                  <figcaption className="overlay">
                    <a href="img/portfolio-1.jpg" className="img_popup"></a>
                  </figcaption>
                </figure>
              </li>
              <li className="col-lg-3 col-md-6 padding-15 single_item photo">
                <figure className="portfolio_item">
                  <img src={require('./../img/promo-1.jpg')} alt="Portfolio Item" />
                  <figcaption className="overlay">
                    <a href="img/portfolio-2.jpg" className="img_popup"></a>
                  </figcaption>
                </figure>
              </li>
              <li className="col-lg-3 col-md-6 padding-15 single_item print branding">
                <figure className="portfolio_item">
                  <img src={require('./../img/promo-3.jpg')} alt="Portfolio Item" />
                  <figcaption className="overlay">
                    <a href="img/portfolio-3.jpg" className="img_popup"></a>
                  </figcaption>
                </figure>
              </li>
              <li className="col-lg-3 col-md-6 padding-15 single_item website photo">
                <figure className="portfolio_item">
                  <img src={require('./../img/promo-1.jpg')} alt="Portfolio Item" />
                  <figcaption className="overlay">
                    <a href="img/portfolio-4.jpg" className="img_popup"></a>
                  </figcaption>
                </figure>
              </li>
              <li className="col-lg-3 col-md-6 padding-15 single_item print photo">
                <figure className="portfolio_item">
                  <img src={require('./../img/promo-3.jpg')} alt="Portfolio Item" />
                  <figcaption className="overlay">
                    <a href="img/portfolio-5.jpg" className="img_popup"></a>
                  </figcaption>
                </figure>
              </li>
              <li className="col-lg-6 col-md-6 padding-15 single_item branding website">
                <figure className="portfolio_item">
                  <img src={require('./../img/hero-1.jpg')} alt="Portfolio Item" />
                  <figcaption className="overlay">
                    <a href="img/portfolio-6.jpg" className="img_popup"></a>
                  </figcaption>
                </figure>
              </li>
            </ul>
          </div>
        </section>
        <CtaSection />
        <Footer />
      </div>
    );
  }
}

export default Gallery;
