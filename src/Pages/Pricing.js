import React, { Component } from 'react';
import Header from './../Components/Header';
import PageHeader from './../Components/PageHeader';
import CtaSection from './../Components/CtaSection';
import Footer from './../Components/Footer';
import './../main.css';
import './../Style/Animate_min.css';
import './../Style/Barber_icon.css';
import './../Style/Bootstrap_min.css';
import './../Style/Button.css';
import './../Style/Elegant_font_icons.css';
import './../Style/Elegant_line_icons.css';
import './../Style/Nice_select.css';
import './../Style/owl.carousel.css';
import './../Style/Responsive.css';
import './../Style/slicknav.min.css';
import './../Style/themify-icons.css';

class Pricing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pricingData: [
        {
          category: "Hair Styling",
          items: [
            { title: "Hair Cut", description: "Barber is a person whose occupation is mainly to cut dress groom style and shave men.", price: "$8" },
            { title: "Hair Styling", description: "Barber is a person whose occupation is mainly to cut dress groom style and shave men.", price: "$9" },
            { title: "Hair Triming", description: "Barber is a person whose occupation is mainly to cut dress groom style and shave men.", price: "$10" },
          ],
        },
        {
          category: "Shaving",
          items: [
            { title: "Clean Shaving", description: "Barber is a person whose occupation is mainly to cut dress groom style and shave men.", price: "$8" },
            { title: "Beard Triming", description: "Barber is a person whose occupation is mainly to cut dress groom style and shave men.", price: "$9" },
            { title: "Smooth Shave", description: "Barber is a person whose occupation is mainly to cut dress groom style and shave men.", price: "$10" },
          ],
        },
        {
          category: "Face Masking",
          items: [
            { title: "White Facial", description: "Barber is a person whose occupation is mainly to cut dress groom style and shave men.", price: "$8" },
            { title: "Face Cleaning", description: "Barber is a person whose occupation is mainly to cut dress groom style and shave men.", price: "$9" },
            { title: "Bright Tuning", description: "Barber is a person whose occupation is mainly to cut dress groom style and shave men.", price: "$10" },
          ],
        },
      ],
    };
  }

  render() {
    return (
      <div>
        <Header />
        <PageHeader />
        <section className="pricing_section bg-grey bd-bottom padding">
          <div className="container">
            <div className="row">
              {this.state.pricingData.map((category, index) => (
                <div key={index} className="col-lg-4 col-md-6 sm-padding">
                  <div className="price_wrap">
                    <h3>{category.category}</h3>
                    <ul className="price_list">
                      {category.items.map((item, idx) => (
                        <li key={idx}>
                          <h4>{item.title}</h4>
                          <p>{item.description}</p>
                          <span className="price">{item.price}</span>
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </section>
        <CtaSection />
        {/* Bottom slider */}
        <Footer />
      </div>
    );
  }
}

export default Pricing;
