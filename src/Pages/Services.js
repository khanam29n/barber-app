import React, { Component } from 'react';
import Header from './../Components/Header';
import CtaSection from './../Components/CtaSection';
import Footer from './../Components/Footer';
import './../main.css';
import './../Style/Animate_min.css';
import './../Style/Barber_icon.css';
import './../Style/Bootstrap_min.css';
import './../Style/Button.css';
import './../Style/Elegant_font_icons.css';
import './../Style/Elegant_line_icons.css';
import './../Style/Nice_select.css';
import './../Style/owl.carousel.css';
import './../Style/Responsive.css';
import './../Style/slicknav.min.css';
import './../Style/themify-icons.css';

class Services extends Component {
  render() {
    return (
      <div>
        <Header />
        <section className="page_header d-flex align-items-center">
          <div className="container">
            <div
              className="section_heading text-center mb-40 wow fadeInUp"
              data-wow-delay="300ms"
            >
              <h3>Trendy Salon &amp; Spa</h3>
              <h2>Barbershop Services</h2>
              <div className="heading-line"></div>
            </div>
          </div>
        </section>
        <section className="service_section bg-grey padding">
          <div className="container">
            <div className="row">
              <div
                className="col-lg-3 col-md-6 padding-15 wow fadeInUp"
                data-wow-delay="200ms"
              >
                <div className="service_box">
                  <i className="bs bs-scissors-1"></i>
                  <h3>Haircut Styles</h3>
                  <p>
                    Barber is a person whose occupation is mainly to cut dress
                    style.
                  </p>
                </div>
              </div>
              <div
                className="col-lg-3 col-md-6 padding-15 wow fadeInUp"
                data-wow-delay="300ms"
              >
                <div className="service_box">
                  <i className="bs bs-razor-2"></i>
                  <h3>Beard Triming</h3>
                  <p>
                    Barber is a person whose occupation is mainly to cut dress
                    style.
                  </p>
                </div>
              </div>
              <div
                className="col-lg-3 col-md-6 padding-15 wow fadeInUp"
                data-wow-delay="400ms"
              >
                <div className="service_box">
                  <i className="bs bs-brush"></i>
                  <h3>Smooth Shave</h3>
                  <p>
                    Barber is a person whose occupation is mainly to cut dress
                    style.
                  </p>
                </div>
              </div>
              <div
                className="col-lg-3 col-md-6 padding-15 wow fadeInUp"
                data-wow-delay="500ms"
              >
                <div className="service_box">
                  <i className="bs bs-hairbrush-1"></i>
                  <h3>Face Masking</h3>
                  <p>
                    Barber is a person whose occupation is mainly to cut dress
                    style.
                  </p>
                </div>
              </div>
              <div
                className="col-lg-3 col-md-6 padding-15 wow fadeInUp"
                data-wow-delay="600ms"
              >
                <div className="service_box">
                  <i className="bs bs-razor"></i>
                  <h3>Beard Triming</h3>
                  <p>
                    Barber is a person whose occupation is mainly to cut dress
                    style.
                  </p>
                </div>
              </div>
              <div
                className="col-lg-3 col-md-6 padding-15 wow fadeInUp"
                data-wow-delay="700ms"
              >
                <div className="service_box">
                  <i className="bs bs-hairbrush"></i>
                  <h3>Hair Coloring</h3>
                  <p>
                    Barber is a person whose occupation is mainly to cut dress
                    style.
                  </p>
                </div>
              </div>
              <div
                className="col-lg-3 col-md-6 padding-15 wow fadeInUp"
                data-wow-delay="800ms"
              >
                <div className="service_box">
                  <i className="bs bs-comb-2"></i>
                  <h3>Hair Straight</h3>
                  <p>
                    Barber is a person whose occupation is mainly to cut dress
                    style.
                  </p>
                </div>
              </div>
              <div
                className="col-lg-3 col-md-6 padding-15 wow fadeInUp"
                data-wow-delay="900ms"
              >
                <div className="service_box">
                  <i className="bs bs-perfume"></i>
                  <h3>Bright Facial</h3>
                  <p>
                    Barber is a person whose occupation is mainly to cut dress
                    style.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <CtaSection />
        {/* <Sponsor /> */}
        <Footer />
      </div>
    );
  }
}

export default Services;
