import React, { Component } from 'react';
import Header from './../Components/Header';
import PageHeader from './../Components/PageHeader';
import CtaSection from './../Components/CtaSection';
import Footer from './../Components/Footer';
import './../main.css';
import './../Style/Animate_min.css';
import './../Style/Barber_icon.css';
import './../Style/Bootstrap_min.css';
import './../Style/Button.css';
import './../Style/Elegant_font_icons.css';
import './../Style/Elegant_line_icons.css';
import './../Style/Nice_select.css';
import './../Style/owl.carousel.css';
import './../Style/Responsive.css';
import './../Style/slicknav.min.css';
import './../Style/themify-icons.css';

class OurTeam extends Component {
  render() {
    const teamMembers = [
      {
        name: 'Kyle Frederick',
        role: 'WEB DESIGNER',
        image: require('./../img/post-1.jpg'),
      },
      {
        name: 'José Carpio',
        role: 'WORDPRESS DEVELOPER',
        image: require('./../img/post-2.jpg'),
      },
      {
        name: 'Michel Ibáñez',
        role: 'ONLINE MARKETER',
        image: require('./../img/post-4.jpg'),
      },
      {
        name: 'Adam Castellon',
        role: 'JAVA SPECIALIST',
        image: require('./../img/post-6.jpg'),
      },
    ];

    return (
      <div>
        <Header />
        <PageHeader />
        <section id="team" className="team_section bd-bottom padding">
          <div className="container">
            <ul className="team_members row">
              {teamMembers.map((teamMember, index) => (
                <li
                  key={index}
                  className="col-lg-3 col-md-6 sm-padding wow fadeInUp"
                  data-wow-delay={`${200 + 100 * index}ms`}
                >
                  <div className="team_member">
                    <img src={teamMember.image} alt="Team Member" />
                    <div className="overlay">
                      <h3>{teamMember.name}</h3>
                      <p>{teamMember.role}</p>
                    </div>
                  </div>
                </li>
              ))}
            </ul>
          </div>
        </section>
        <CtaSection />
        <Footer />
      </div>
    );
  }
}

export default OurTeam;
