import React, { Component } from 'react';
import Header from './../Components/Header';
import Slider from './../Components/Slider';
import AboutUs from './../Components/AboutUs';
import Service from './../Components/Service';
import BookSection from './../Components/BookSection';
import TeamSection from './../Components/TeamSection';
import Reviews from './../Components/Reviews';
import PriceTag from './../Components/Price_tag';
import CtaSection from './../Components/CtaSection';
import BlogSection from './../Components/BlogSection';
import Footer from './../Components/Footer';
import './../App.css';
import './../main.css';
import './../Style/Animate_min.css';
import './../Style/Barber_icon.css';
import './../Style/Bootstrap_min.css';
import './../Style/Button.css';
import './../Style/Elegant_font_icons.css';
import './../Style/Elegant_line_icons.css';
import './../Style/Nice_select.css';
import './../Style/owl.carousel.css';
import './../Style/Responsive.css';
import './../Style/slicknav.min.css';
import './../Style/themify-icons.css';

class Home extends Component {
  render() {
    return (
      <div>
        <Header />
        <Slider />
        <AboutUs />
        <Service />
        <BookSection />
        <TeamSection />
        <Reviews />
        <PriceTag />
        <CtaSection />
        <BlogSection />
        {/* <Sponsor /> */}
        <Footer />
      </div>
    );
  }
}

export default Home;
