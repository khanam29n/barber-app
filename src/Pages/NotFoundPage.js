import React, { Component } from 'react';
import Header from '../Components/Header';
import Footer from '../Components/Footer';
import './../main.css';
import './../Style/Animate_min.css';
import './../Style/Barber_icon.css';
import './../Style/Bootstrap_min.css';
import './../Style/Button.css';
import './../Style/Elegant_font_icons.css';
import './../Style/Elegant_line_icons.css';
import './../Style/Nice_select.css';
import './../Style/owl.carousel.css';
import './../Style/Responsive.css';
import './../Style/slicknav.min.css';
import './../Style/themify-icons.css';

class NotFoundPage extends Component {
  render() {
    return (
      <div>
        <Header />
        <section className="not_found_section padding">
          <div className="container">
            <div className="404_content align-center">
              <img className="mb-20" src={require('./../img/not-found.png')} alt="404" />
              <h2>404 Error Not Found</h2>
              <p>
                The Page Looking for is not here. We can’t find the page you’re looking for. <br /> Check our our
                Help Center or head back to home.
              </p>
              <a href="index.html" className="default_btn">
                Back To Home
              </a>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

export default NotFoundPage;
