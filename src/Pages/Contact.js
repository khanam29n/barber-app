import React, { Component } from 'react';
import Header from './../Components/Header';
import Footer from './../Components/Footer';
import './../main.css';
import './../Style/Animate_min.css';
import './../Style/Barber_icon.css';
import './../Style/Bootstrap_min.css';
import './../Style/Button.css';
import './../Style/Elegant_font_icons.css';
import './../Style/Elegant_line_icons.css';
import './../Style/Nice_select.css';
import './../Style/owl.carousel.css';
import './../Style/Responsive.css';
import './../Style/slicknav.min.css';
import './../Style/themify-icons.css';

class Contact extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="mapouter">
          <div className="gmap_canvas">
            <iframe
              width="100%"
              height="350"
              id="gmap_canvas"
              src="https://maps.google.com/maps?q=Dynamic%20Layers&t=&z=11&ie=UTF8&iwloc=&output=embed"
              frameBorder="0"
              scrolling="no"
              marginHeight="0"
              marginWidth="0"
            ></iframe>
            <a href="https://www.emojilib.com"></a>
          </div>
        </div>

        <section className="contact-section padding">
          <div className="map"></div>
          <div className="container">
            <div className="contact-wrap d-flex align-items-center row">
              <div className="col-lg-6 sm-padding">
                <div className="contact-info">
                  <h2>
                    Get in touch with us & <br />
                    send us message today!
                  </h2>
                  <p>
                    Saasbiz is a different kind of architecture practice. Founded
                    by LoganCee in 1991, we’re an employee-owned firm pursuing a
                    democratic design process that values everyone’s input.
                  </p>
                  <h3>
                    198 West 21th Street, Suite 721 <br />
                    New York, NY 10010
                  </h3>
                  <h4>
                    <span>Email:</span> Dynamiclayers.Net <br />{" "}
                    <span>Phone:</span> +88 (0) 101 0000 000 <br />{" "}
                    <span>Fax:</span> +88 (0) 202 0000 001
                  </h4>
                </div>
              </div>
              <div className="col-lg-6 sm-padding">
                <div className="contact-form">
                  <form
                    action="contact.php"
                    method="post"
                    id="ajax_form"
                    className="form-horizontal"
                  >
                    <div className="form-group colum-row row">
                      <div className="col-sm-6">
                        <input
                          type="text"
                          id="name"
                          name="name"
                          className="form-control"
                          placeholder="Name"
                          required
                        />
                      </div>
                      <div className="col-sm-6">
                        <input
                          type="email"
                          id="email"
                          name="email"
                          className="form-control"
                          placeholder="Email"
                          required
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <div className="col-md-12">
                        <textarea
                          id="message"
                          name="message"
                          cols="30"
                          rows="5"
                          className="form-control message"
                          placeholder="Message"
                          required
                        ></textarea>
                      </div>
                    </div>
                    <div className="form-group row">
                      <div className="col-md-12">
                        <button id="submit" className="default_btn" type="submit">
                          Send Message
                        </button>
                      </div>
                    </div>
                    <div id="form-messages" className="alert" role="alert"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

export default Contact;
