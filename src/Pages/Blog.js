import React from "react";
import { Link } from 'react-router-dom';
import "./../../src/App.css";
import "./../main.css";
import "./../Style/Animate_min.css";
import "./../Style/Barber_icon.css";
import "./../Style/Bootstrap_min.css";
import "./../Style/Button.css";
import "./../Style/Elegant_font_icons.css";
import "./../Style/Elegant_line_icons.css";
import "./../Style/Nice_select.css";
import "./../Style/owl.carousel.css";
import "./../Style/Responsive.css";
import "./../Style/slicknav.min.css";
import "./../Style/themify-icons.css";
import Header from "./../Components/Header";
import PageHeader from "../Components/PageHeader";
import Footer from "./../Components/Footer";
import Sidebar from "./../Components/Sidebar";


const Blog = () => {
  const blogPosts = [
    {
      id: 1,
      imageUrl: "http://localhost:3000/static/media/post-1.fd26a1e72b16ce38d4a1.jpg",
      title: "Minimalist trending in modern architecture 2019",
      category: "interior",
      content:
        "Building first evolved out dynamics between needs means available building materials attendant skills.Home renovations, especially those involving plentiful of demolition can be a very dusty affair. This nasty dust can easily free flow through the air and into your house.",
    },
    {
      id: 2,
      imageUrl: "http://localhost:3000/static/media/post-2.d83db3bfc7394d2fdade.jpg",
      title: "Terrace in the town yamazaki kentaro design workshop",
      category: "ARCHITECTURE",
      content:
        "Building first evolved out dynamics between needs means available building materials attendant skills.Home renovations, especially those involving plentiful of demolition can be a very dusty affair. This nasty dust can easily free flow through the air and into your house.",
    },
    {
      id: 3,
      imageUrl: "http://localhost:3000/static/media/post-3.3d1606ac459a10ae2743.jpg",
      title: "W270 house são paulo arquitetos fabio jorge architeqture.",
      category: "design",
      content:
        "Building first evolved out dynamics between needs means available building materials attendant skills.Home renovations, especially those involving plentiful of demolition can be a very dusty affair. This nasty dust can easily free flow through the air and into your house.",
    },
    // Add more blog posts as needed
  ];

  const renderBlogPosts = () => {
    return blogPosts.map((post) => (
      <div key={post.id} className="col-lg-12">
        <div className="blog-item">
          <div className="blog-thumb">
            <img src={post.imageUrl} alt="post" />
            <span className="category">
              <a href="#">{post.category}</a>
            </span>
          </div>
          <div className="blog-content">
            <h3>
              <a href="#">{post.title}</a>
            </h3>
            <p>{post.content}</p>
            <Link to="http://localhost:3000/blogdetails" className="read-more">
              Read More
            </Link>
          </div>
        </div>
      </div>
    ));
  };

  return (
    <div>
      <Header />
      <PageHeader />

      <section className="blog-section padding">
        <div className="container">
          <div className="blog-wrap row">
            <div className="col-lg-8 sm-padding">
              <div className="row blog-classic">{renderBlogPosts()}</div>
              <ul className="pagination-wrap text-left mt-30">
                <li>
                  <a href="#">
                    <i className="ti-arrow-left"></i>
                  </a>
                </li>
                <li>
                  <a href="#">1</a>
                </li>
                <li>
                  <a href="#" className="active">
                    2
                  </a>
                </li>
                <li>
                  <a href="#">3</a>
                </li>
                <li>
                  <a href="#">
                    <i className="ti-arrow-right"></i>
                  </a>
                </li>
              </ul>
              {/* <!-- Pagination --> */}
            </div>
            {/* <!--/.col-lg-8--> */}
            <div className="col-lg-4 sm-padding">
              <Sidebar />
              {/* <!--/.sidebar-wrap--> */}
            </div>
            {/* <!--/.col-lg-4--> */}
          </div>
          {/* <!--/.blog-wrap--> */}
        </div>
      </section>
      <Footer />
    </div>
  );
};

export default Blog;


