import React, {Component} from "react";
import "./../../src/App.css";
import "./../main.css";
import "./../Style/Animate_min.css";
import "./../Style/Barber_icon.css";
import "./../Style/Bootstrap_min.css";
import "./../Style/Button.css";
import "./../Style/Elegant_font_icons.css";
import "./../Style/Elegant_line_icons.css";
import "./../Style/Nice_select.css";
import "./../Style/owl.carousel.css";
import "./../Style/Responsive.css";
import "./../Style/slicknav.min.css";
import "./../Style/themify-icons.css";
import Header from "./../Components/Header";
import Footer from "./../Components/Footer";


export default class BlogDetails extends Component {
  state = {
    post: {
      id: 1,
      imageUrl:
        "http://localhost:3000/static/media/post-single.4b4b5ec6b332a7e5e96e.jpg",
      title: "Minimalist trending in modern architecture 2019",
      categories: ["interior", "building", "design"],
      content: (
        <>
          <p>
            Architectural phenomenology is a movement within architecture that
            began in the 1950s, reaching a wide audience in the late 1970s and
            1980s, and continuing until today. Architectural phenomenology
            focuses on human experience, background, intention and historical
            reflection, interpretation as well as poetic and ethical
            considerations with authors such as Gaston Bachelard.
          </p>
          <p>
            Islamic architecture began in the 7th century CE, incorporating
            architectural forms from the ancient Middle East and Byzantium, but
            also developing features to suit the religious and social needs of
            the society. Examples can be found throughout the Middle East, North
            Africa, Spain and the Indian Sub-continent.
          </p>
          <blockquote>
            <div className="dots"></div>
            <p>
              “Architecture is really about well-being. I think that people want
              to feel good in a space… On the one hand it’s about shelter, but
              it’s also about pleasure.”
            </p>
            <span className="quoter">- Dr. Anders Ericsson</span>
          </blockquote>
          <p>
            The most important aspect of beauty was, therefore, an inherent part
            of an object, rather than something applied superficially, and was
            based on universal, recognisable truths. The notion of style in the
            arts was not developed until the 16th century, with the writing of
            Vasari:[11] by the 18th century, his Lives of the Most Excellent
            Painters, Sculptors, and Architects had been translated into
            Italian, French, Spanish, and English.
          </p>
        </>
      ),
    },
    comments: [
      {
        title: "Jhon Castellon ",
        image:
          "http://localhost:3000/static/media/comment-1.cd17f0f686c176cf4142.png",
        date: "JAN 05, 2020 AT 8:00",
        description:
          "Home renovations, especially those involving plentiful of demolition can be a very dusty affair. This nasty dust can easily free flow through your house.",
      },
      {
        title: "José Carpio",
        image:
          "http://localhost:3000/static/media/comment-2.8bc5d59b0a411b542c7d.png",
        date: "JAN 15, 2020 AT 8:00",
        description:
          "Home renovations, especially those involving plentiful of demolition can be a very dusty affair. This nasty dust can easily free flow through your house.",
      },
      {
        title: "Valentin Lacoste",
        image:
          "http://localhost:3000/static/media/comment-3.bda45b6de01b8d812175.png",
        date: "JAN 25, 2020 AT 8:00",
        description:
          "Home renovations, especially those involving plentiful of demolition can be a very dusty affair. This nasty dust can easily free flow through your house.",
      },
      {
        title: "Kyle Frederick",
        image:
          "http://localhost:3000/static/media/comment-4.f8fcb67bca099fc14017.png",
        date: "JAN 02, 2020 AT 8:00",
        description:
          "Home renovations, especially those involving plentiful of demolition can be a very dusty affair. This nasty dust can easily free flow through your house.",
      },
    ],
  };
  render() {
    return (
      <div>
        <Header />
        <section className="page_header d-flex align-items-center">
          <div className="container">
            <div
              className="section_heading text-center mb-40 wow fadeInUp"
              data-wow-delay="300ms"
            >
              <h3>Trendy Salon &amp; Spa</h3>
              <h2>Barbershop News</h2>
              <div className="heading-line"></div>
            </div>
          </div>
        </section>
        <section className="blog-section padding">
          <div className="container">
            <div className="blog-wrap row">
              <div className="col-lg-8 sm-padding">
                <div className="blog-single-wrap">
                  <div className="blog-thumb">
                    {/* {this.state.post.imageUrl} */}
                    <img src={require("./../img/post-single.jpg")} alt="img" />
                    {/* <img src={this.state.post.imageUrl} alt="img" /> */}
                  </div>
                  <div className="blog-single-content">
                    <h2>{this.state.post.title}</h2>
                    {/* <h2>
                      <a href="#">
                        Minimalist trending in modern architecture 2019
                      </a>
                    </h2> */}
                    <ul className="single-post-meta">
                      <li>
                        <i className="fa fa-user"></i> <a href="#">Admin</a>
                      </li>
                      <li>
                        <i className="fa fa-calendar"></i>{" "}
                        <a href="#">19 Feb, 2019</a>
                      </li>
                      <li>
                        <i className="fa fa-comments"></i>{" "}
                        <a href="#">2 Comments</a>
                      </li>
                    </ul>

                    {this.state.post.content}

                    <ul className="post-tags">
                      {this.state.post.categories.map((el, index) => {
                        return (
                          <li>
                            <a href="#">{el}</a>
                          </li>
                        );
                      })}
                    </ul>
                    <div className="author-box bg-grey">
                      <img src={require("./../img/comment-3.png")} alt="img" />
                      <div className="author-info">
                        <h3>Albert Nouwen</h3>
                        <p>
                          Barbershop is a different kind of architecture
                          practice. Founded by LoganCee in 1991, we’re an
                          employee-owned firm pursuing a democratic design.
                        </p>
                        <ul className="social-icon">
                          <li>
                            <a href="#">
                              <i className="ti-facebook"></i>
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i className="ti-twitter"></i>
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i className="ti-instagram"></i>
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i className="ti-pinterest"></i>
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i className="ti-youtube"></i>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="post-navigation row">
                      <div className="col prev-post">
                        <a href="#">
                          <i className="ti-arrow-left"></i>Prev Post
                        </a>
                      </div>
                      <div className="col next-post">
                        <a href="#">
                          Next Post <i className="ti-arrow-right"></i>
                        </a>
                      </div>
                    </div>
                    <div className="comments-area">
                      <div className="comments-section">
                        <h3 className="comments-title">Posts Comments</h3>

                        <div>
                          {this.state.comments.map((comment, index) => (
                            <div key={index}>
                              <h4>{comment.title}</h4>
                              <img src={comment.image} alt="Comment" />
                              <p>Date: {comment.date}</p>
                              <p>{comment.description}</p>
                            </div>
                          ))}
                        </div>
                        <ol className="comments">
                          <li
                            className="comment even thread-even depth-1"
                            id="comment-1"
                          >
                            <div id="div-comment-1">
                              <div className="comment-thumb">
                                <div className="comment-img">
                                  <img
                                    src={require("./../img/comment-1.png")}
                                    alt=""
                                  />
                                </div>
                              </div>
                              <div className="comment-main-area">
                                <div className="comment-wrapper">
                                  <div className="comments-meta">
                                    <h4>
                                      Jhon Castellon{" "}
                                      <span className="comments-date">
                                        jan 05, 2020 at 8:00
                                      </span>
                                    </h4>
                                  </div>
                                  <div className="comment-area">
                                    <p>
                                      Home renovations, especially those
                                      involving plentiful of demolition can be a
                                      very dusty affair. This nasty dust can
                                      easily free flow through your house.
                                    </p>
                                    <div className="comments-reply">
                                      <a
                                        className="comment-reply-link"
                                        href="#"
                                      >
                                        <span>Reply</span>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <ul className="children">
                              <li className="comment">
                                <div>
                                  <div className="comment-thumb">
                                    <div className="comment-img">
                                      <img
                                        src={require("./../img/comment-2.png")}
                                        alt=""
                                      />
                                    </div>
                                  </div>
                                  <div className="comment-main-area">
                                    <div className="comment-wrapper">
                                      <div className="comments-meta">
                                        <h4>
                                          José Carpio{" "}
                                          <span className="comments-date">
                                            jan 15, 2020 at 8:00
                                          </span>
                                        </h4>
                                      </div>
                                      <div className="comment-area">
                                        <p>
                                          Home renovations, especially those
                                          involving plentiful of demolition can
                                          be a very dusty affair. This nasty
                                          dust can easily free flow through your
                                          house.
                                        </p>
                                        <div className="comments-reply">
                                          <a
                                            className="comment-reply-link"
                                            href="#"
                                          >
                                            <span>Reply</span>
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <ul>
                                  <li className="comment">
                                    <div>
                                      <div className="comment-thumb">
                                        <div className="comment-img">
                                          <img
                                            src={require("./../img/comment-3.png")}
                                            alt=""
                                          />
                                        </div>
                                      </div>
                                      <div className="comment-main-area">
                                        <div className="comment-wrapper">
                                          <div className="comments-meta">
                                            <h4>
                                              Valentin Lacoste{" "}
                                              <span className="comments-date">
                                                jan 25, 2020 at 8:00
                                              </span>
                                            </h4>
                                          </div>
                                          <div className="comment-area">
                                            <p>
                                              Home renovations, especially those
                                              involving plentiful of demolition
                                              can be a very dusty affair. This
                                              nasty dust can easily free flow
                                              through your house.
                                            </p>
                                            <div className="comments-reply">
                                              <a
                                                className="comment-reply-link"
                                                href="#"
                                              >
                                                <span>Reply</span>
                                              </a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                </ul>
                              </li>
                            </ul>
                          </li>

                          <li className="comment">
                            <div>
                              <div className="comment-thumb">
                                <div className="comment-img">
                                  <img
                                    src={require("./../img/comment-4.png")}
                                    alt=""
                                  />
                                </div>
                              </div>
                              <div className="comment-main-area">
                                <div className="comment-wrapper">
                                  <div className="comments-meta">
                                    <h4>
                                      Kyle Frederick{" "}
                                      <span className="comments-date">
                                        jan 02, 2020 at 8:00
                                      </span>
                                    </h4>
                                  </div>
                                  <div className="comment-area">
                                    <p>
                                      Home renovations, especially those
                                      involving plentiful of demolition can be a
                                      very dusty affair. This nasty dust can
                                      easily free flow through your house.
                                    </p>
                                    <div className="comments-reply">
                                      <a
                                        className="comment-reply-link"
                                        href="#"
                                      >
                                        <span>Reply</span>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </li>
                        </ol>
                      </div>

                      <div className="comment-respond">
                        <h3 className="comment-reply-title">Write a Comment</h3>
                        <form
                          method="post"
                          id="commentform"
                          className="comment-form"
                        >
                          <div>
                            <div className="form-textarea">
                              <textarea
                                id="comment"
                                placeholder="Write Your Comments..."
                              ></textarea>
                            </div>
                            <div className="form-inputs">
                              <input placeholder="Website" type="url" />
                              <input placeholder="Name" type="text" />
                              <input placeholder="Email" type="email" />
                            </div>
                            <div className="form-submit">
                              <input
                                id="submit"
                                value="Post Comment"
                                type="submit"
                              />
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 sm-padding">
                <div className="sidebar-wrap">
                  <div className="widget-content">
                    <form action="" className="search-form">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Type here"
                      />
                      <button className="search-btn" type="button">
                        <i className="fa fa-search"></i>
                      </button>
                    </form>
                  </div>
                  <div className="widget-content">
                    <h4>Categories</h4>
                    <ul className="widget-links">
                      <li>
                        <a href="#">Architecture</a>
                      </li>
                      <li>
                        <a href="#">Interior Design</a>
                      </li>
                      <li>
                        <a href="#">Designing</a>
                      </li>
                      <li>
                        <a href="#">Construction</a>
                      </li>
                      <li>
                        <a href="#">Buildings</a>
                      </li>
                    </ul>
                  </div>
                  <div className="widget-content">
                    <h4>Recent Posts</h4>
                    <ul className="thumb-post">
                      <li>
                        <img src={require("./../img/post-1.jpg")} alt="post" />
                        <a href="#">
                          Minimalist trending in modern architecture 2019
                        </a>
                      </li>
                      <li>
                        <img src={require("./../img/post-2.jpg")} alt="post" />
                        <a href="#">
                          Terrace in the town kentaro design workshop.
                        </a>
                      </li>
                      <li>
                        <img src={require("./../img/post-3.jpg")} alt="post" />
                        <a href="#">
                          W270 house são arquitetos fabio architeqture.
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="widget-content">
                    <h4>Tag Clouds</h4>
                    <ul className="tags">
                      <li>
                        <a href="#">Architecture</a>
                      </li>
                      <li>
                        <a href="#">Interior Design</a>
                      </li>
                      <li>
                        <a href="#">Designing</a>
                      </li>
                      <li>
                        <a href="#">Construction</a>
                      </li>
                      <li>
                        <a href="#">Buildings</a>
                      </li>
                      <li>
                        <a href="#">Industrial</a>
                      </li>
                      <li>
                        <a href="#">Factory</a>
                      </li>
                      <li>
                        <a href="#">Material</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <Footer />
      </div>
    );
  }
}
